//Lukasz Dzwoniarek
//274404

#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <fstream>
#include <sys/time.h>

#include "com.h"
#include "data.h"
#include "debug.h"

using namespace std;


int main(int argc, char **argv){
    cout << "Start" << endl;

    time_t run_time = time(NULL);

    char file_name[100] = "my";  //Zmienne do wczytania
    int port = 40008;
    int file_length = 900455;

    if(argc!=4)
        error("Podaj port, nazwe pliku wynikowego i dlugosc pliku\n");
    sscanf(argv[1], "%d", &port);
    if(port<0)
        error("Port musi byc wiekszy od 0\n");
    sscanf(argv[2], "%s", file_name);
    if(fn_test(file_name))
        error("Nazwa pliku nie moze zawierac znaku ""/""\n");
    sscanf(argv[3], "%d", &file_length);
    if(file_length<0 || file_length>1000000)
        error("Dlougosc pliku musi byc z zakresu od 0 do 1000000\n");

    ofstream myfile;
    myfile.open (file_name,ios::out|ios::binary);

    char nazwa[]="aisd.ii.uni.wroc.pl";
    print(nazwa);

    int s = socket_new();

    struct sockaddr_in si_other;
    bzero(&si_other, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(port);
    si_other.sin_addr.s_addr = name_to_ip(nazwa);


    char data_get[100];
    char buffer[1500];
    int number=0, length=0;

    int bfl = 100;
    struct s_bfile *bfile = (s_bfile *)calloc(bfl, sizeof(s_bfile));

    int sn=0;  //send number - numer kolejnego pakietu du wysylki

    for(; sn*1000<file_length && sn<bfl; sn++){
        if( (sn+1)*1000 <= file_length ){
            sprintf(data_get, "GET %d %d\n", sn*1000, 1000);
        }
        else{
            sprintf(data_get, "GET %d %d\n", sn*1000, file_length%1000);
        }
        print(data_get);
        send_data(s, &si_other, data_get);
        bfile[sn].number = sn;
        bfile[sn].recived = false;
        gettimeofday(&bfile[sn].ts, NULL);
        if(recive_data_now(s,buffer, sizeof(buffer), &si_other) == 1){
            sscanf(buffer, "DATA %d %d", &number, &length);
            if(bfile[number/1000].recived == false && number<=sn*1000){
                bfile[number/1000].recived = true;
                bfile[number/1000].length = length;
                print("recived: %d %d", number/1000, length);
                copy_buffer(bfile[number/1000].buffer, buffer);
            }
        }
    }


    //ODBIERANIE
    //rn - recived number - numer nastepnego pakietu do odebrania


    for(int rn=0; rn*1000<file_length; ){
        if(recive_data(s,buffer, sizeof(buffer), &si_other) == 1){
            sscanf(buffer, "DATA %d %d", &number, &length);
            if(bfile[(number/1000)%bfl].recived == false && bfile[(number/1000)%bfl].number == number/1000){
                bfile[(number/1000)%bfl].number = number/1000;
                bfile[(number/1000)%bfl].length = length;
                bfile[(number/1000)%bfl].recived = true;
                print("recived: %d %d", number/1000, length);
                copy_buffer(bfile[(number/1000)%bfl].buffer, buffer);
            }
        }
        while(bfile[rn%bfl].recived == true && bfile[rn%bfl].number == rn){
            printf("saved: %d %d\n", bfile[rn%bfl].number, bfile[rn%bfl].length);

            try{
            myfile.write(bfile[rn%bfl].buffer, bfile[rn%bfl].length);
            }
            catch (int e)
            {
            cout << "An exception occurred. Exception Nr. " << e << '\n';
            }

            if(sn*1000<file_length){
                bfile[rn%bfl].number = sn;
                bfile[rn%bfl].recived = false;
                sn++;
            }
            rn++;
        }
        for(int j=rn; j*1000<file_length; j++){
            if(ct(bfile[j%bfl].ts) && bfile[j%bfl].recived == false){
                if( (j+1)*1000 <= file_length ){
                    sprintf(data_get, "GET %d %d\n", j*1000, 1000);
                }
                else{
                    sprintf(data_get, "GET %d %d\n", j*1000, file_length%1000);
                }
                print(data_get);
                send_data(s, &si_other, data_get);
                gettimeofday(&bfile[j%bfl].ts, NULL);
            }
        }
    }


    myfile.close();
    cout << "Time:  " << time(NULL)-run_time << " sec" << endl;
    cout << "File saved" << endl;
    return 0;
}
