CC=g++
CFLAGS=-c -Wall -W

all: client-udp

client-udp: main.o com.o data.o debug.o
	$(CC) main.o com.o data.o debug.o -o client-udp

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp

com.o: com.h com.cpp
	$(CC) $(CFLAGS) com.cpp

data.o: data.h data.cpp
	$(CC) $(CFLAGS) data.cpp

debug.o: debug.h debug.cpp
	$(CC) $(CFLAGS) debug.cpp
distclean:
	rm *.o client-udp
clean:
	rm *o


