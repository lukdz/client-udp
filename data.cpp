//Lukasz Dzwoniarek
//274404

#include <cstdio>
#include <cstring>
#include <sys/time.h>


void copy_buffer(char *destination, const char *source){
    int length=0;
    while(source[length++] != '\n'){}
    memcpy(destination, &source[length], 1000);
}


bool ct(timeval ts){
    timeval now;
    gettimeofday(&now, NULL);
    if(ts.tv_sec == now.tv_sec){
        return ts.tv_usec+10000 < now.tv_usec;
    }
    if(ts.tv_sec+1 == now.tv_sec){
        return ts.tv_usec < now.tv_usec+990000;
    }
    return true;
}

bool fn_test(char *name){
    for(int i=0; ; i++){
        if(name[i]=='/')
            return true;
        if(name[i]=='\0')
            return false;
    }
}
