//Lukasz Dzwoniarek
//274404

int socket_new();

unsigned long name_to_ip(char *domain_name);

void send_data(int s, sockaddr_in *ip_str, char *data);

int recive_data(int s, char *buffer, int buf_length, struct sockaddr_in *dst);

int recive_data_now(int s, char *buffer, int buf_length, struct sockaddr_in *dst);
