//Lukasz Dzwoniarek
//274404


#include <cstdio>
#include <cstdlib>
#include <netdb.h>
#include <arpa/inet.h>
#include <cstring>
#include <errno.h>
#include <ctime>
#include <sys/time.h>
#include <netinet/in.h>


#include "com.h"

#include "debug.h"


int socket_new(){
    print("socket_new");

    struct sockaddr_in si_me;
    int s;
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
        error("socket");

    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    if (setsockopt (s, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        error("setsockopt failed\n");

    bzero(&si_me, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = INADDR_ANY;
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(s, (struct sockaddr*)&si_me, sizeof(si_me))==-1)
        error("bind");



    return s;
}


unsigned long name_to_ip(char *domain_name){
    struct addrinfo* result;
    struct addrinfo hints;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;		// chcemy tylko adresy IPv4
    hints.ai_socktype = SOCK_STREAM;	// inaczej dostaniemy trzy struktury (dla SOCK_STREAM, SOCK_DGRAM i SOCK_RAW)

    int error = getaddrinfo(domain_name, NULL, &hints, &result);

    if (error != 0){
        fprintf(stderr, "getaddrinfo error: %s\n", (error == EAI_SYSTEM) ? strerror(errno) : "other error");
        exit(1);
    }

    struct sockaddr_in* addr = (struct sockaddr_in*)(result->ai_addr);
    unsigned long ip = addr->sin_addr.s_addr;
    char ip_address2[20];
    inet_ntop (AF_INET, &(addr->sin_addr), ip_address2, sizeof(ip_address2));
    print(ip_address2);

    freeaddrinfo(result);
    return ip;
}


void send_data(int s, sockaddr_in *ip_str, char *data){
    print("send_data");

    int length=0;
    while(data[length++] != '\n'){}
    //printf("length: %d\n", length);

    print("Sending packet");
    if (sendto(s, data, length, 0, (struct sockaddr*)ip_str, (socklen_t)sizeof(sockaddr_in))==-1)
        error("sendto()");
    print("Sending complite");
}

int recive_data(int s, char *buffer, int buf_length, struct sockaddr_in *dst){
    print("recvfrom()");

    struct sockaddr_in rec;
    unsigned int recl = sizeof(sockaddr_in);
    int rf = recvfrom(s, buffer, buf_length, 0, (struct sockaddr*)&rec, (socklen_t*)&recl);


    if(rf == -1){
        print("recived null");
        return 0;
    }
    else{
        print("recived data");
        print("IP:%s\tPORT:%lu", inet_ntoa(rec.sin_addr), ntohs(rec.sin_port) );
        int length=0;
        while(buffer[length++] != '\n'){}
        print("rf: %d\tlength: %d\tdiv: %d", rf, length, rf-length);
        if(rec.sin_addr.s_addr != dst->sin_addr.s_addr || rec.sin_port != dst->sin_port){
            print("recived BAD DATA");
            return 0;
        }
        return 1;
    }
}

int recive_data_now(int s, char *buffer, int buf_length, struct sockaddr_in *dst){
    print("recvfrom()");

    struct sockaddr_in rec;
    unsigned int recl = sizeof(sockaddr_in);
    int rf = recvfrom(s, buffer, buf_length, MSG_DONTWAIT, (struct sockaddr*)&rec, (socklen_t*)&recl);


    if(rf == -1){
        print("recived null");
        return 0;
    }
    else{
        print("recived data");
        print("IP:%s\tPORT:%lu", inet_ntoa(rec.sin_addr), ntohs(rec.sin_port) );
        int length=0;
        while(buffer[length++] != '\n'){}
        print("rf: %d\tlength: %d\tdiv: %d", rf, length, rf-length);
        if(rec.sin_addr.s_addr != dst->sin_addr.s_addr || rec.sin_port != dst->sin_port){
            print("recived BAD DATA");
            return 0;
        }
        return 1;
    }
}

